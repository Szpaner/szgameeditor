#include "drawobject.h"
#include "gamescene.h"
#define qDebug QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO).debug


DrawObject::DrawObject()
{
    id = "";
    x = 0;
    y = 0;
}

void DrawObject::setPixmap(const QPixmap &pixmap)
{
    if(curSprite != sprite)
        delete curSprite;
    curSprite = new QPixmap(pixmap);
    QGraphicsPixmapItem::setPixmap(*curSprite);
}

DrawObject::DrawObject(QString i, int xx, int yy, ColObject* o, GameScene *parent):QObject(parent), id(i), x(xx), y(yy)
{
    path = o->getPath();
    sprite = new QPixmap(path);
    curSprite = sprite;
    width = sprite->width();
    height = sprite->height();
    //bounds = new QRectF(0, 0, w, h);
    QGraphicsPixmapItem::setPixmap(*sprite);
    setAcceptHoverEvents(true);
}

DrawObject::DrawObject(DrawObject& o): QGraphicsPixmapItem(o.parentItem()), id(o.id), x(o.x), y(o.y), path(o.path), width(o.width), height(o.height), sprite(o.sprite)
{
    setPixmap(*sprite);
}

DrawObject::~DrawObject()
{
    delete sprite;
}

QPixmap *DrawObject::getSprite()
{
    return sprite;
}

QRectF DrawObject::boundingRect() const
{
    return QRectF(0, 0, width, height);
}

void DrawObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QGraphicsPixmapItem::paint(painter, option, widget);
}

QString DrawObject::getID()
{
    return id;
}

QString DrawObject::getPath()
{
    return path;
}

int DrawObject::getWidth()
{
    return width;
}

int DrawObject::getHeight()
{
    return height;
}

void DrawObject::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    setCursor(QCursor(Qt::ClosedHandCursor));
    QGraphicsItem::mousePressEvent(event);
}

void DrawObject::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);


    emit boom();
}

void DrawObject::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(QCursor(Qt::OpenHandCursor));
    QGraphicsPixmapItem::hoverMoveEvent(event);
}

void DrawObject::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsPixmapItem::hoverLeaveEvent(event);
}

QVariant DrawObject::itemChange(GraphicsItemChange change, const QVariant & value)
{
    switch(change) {
        case QGraphicsItem::ItemPositionHasChanged:
        case QGraphicsItem::ItemPositionChange:
            GameScene *sc = dynamic_cast<GameScene*>(scene());
            if(sc->isSticky())
            {
                auto tmpX = value.toPoint().x();
                auto tmpY = value.toPoint().y();
                if(tmpX%sc->getGrid()->getSpace() != 0)
                    tmpX = tmpX - tmpX%sc->getGrid()->getSpace();
                if(tmpY%sc->getGrid()->getSpace() != 0)
                    tmpY = tmpY - tmpY%sc->getGrid()->getSpace();

                return QPointF(tmpX, tmpY);
            }
            else
                return QPointF(value.toPointF().x(), value.toPointF().y());
    }
    return QGraphicsItem::itemChange(change, value);
}
