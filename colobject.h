#ifndef COLOBJECT_H
#define COLOBJECT_H

#include <QRectF>
#include <QGraphicsItem>
#include <QPainter>
#include <QPixmap>
#include <QString>
#include <QDebug>

class ColObject : public QGraphicsItem
{
private:

    int w;
    int h;
    QString id;
    QString path;

    //QRectF *bounds;

public:

    ColObject();
    ColObject(QString p);
    ColObject(const ColObject &c);
    ~ColObject();
    QPixmap* icon;
    QString getPath();

    int width();
    int height();

    // QGraphicsItem interface
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // COLOBJECT_H
