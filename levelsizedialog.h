#ifndef LEVELSIZEDIALOG_H
#define LEVELSIZEDIALOG_H

#include <QDialog>
#include "mainwindow.h"
#include "ui_levelsizedialog.h"

class LevelSizeDialog : public QDialog, public Ui::LevelSizeDialog
{
    Q_OBJECT
    MainWindow *p;
    int baseW;
    int baseH;
public:
    LevelSizeDialog(MainWindow *parent = 0);
    ~LevelSizeDialog();
private slots:
    void on_pushOK_clicked();
    void on_pushCancel_clicked();

};

#endif // LEVELSIZEDIALOG_H
