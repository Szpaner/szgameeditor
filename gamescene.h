#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include <QDebug>
#include <QPair>
#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QPalette>
#include <QDateTime>
#include <QGraphicsView>
#include <QDrag>
#include "objectsscroll.h"
#include "drawobject.h"
#include "selectframe.h"
#include "scenegrid.h"

#include "SizeGripItem.h"


class GameScene : public QGraphicsScene
{
    Q_OBJECT
    
    int NOTDRAWING = -1;

    //QVector<QPair<ColObject, QPair<int,int>>> sceneObjectsVect;
    objectsScroll *objectsList;
    QMap<QString, DrawObject*> drawMap;
    QString selectedID;
    //SelectFrame *selectedFrame;
    SceneGrid *grid;
    QRect highlight;
    bool sticky;

public:
    GameScene(QWidget *parent = 0, int w = 960, int h = 540);
    ~GameScene();
    void setObjects(objectsScroll *oS);

    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void leaveEvent(QEvent *event);
    void setGrid(int w, int h);
    void setSticky(bool sticky);
    SceneGrid *getGrid();
    int curObj;

    int x, y;

    bool isSticky();
public slots:
    void setCurObject(int i);
    void boom();

signals:
    void Mouse_Left_Pressed();
    void Mouse_Right_Pressed();
    void Mouse_Pos();
    void Mouse_Left();
};

#endif // GAMESCENE_H
