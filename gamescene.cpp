#include "gamescene.h"
#define qDebug QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO).debug

namespace
{
    class RectResizer : public SizeGripItem::Resizer
    {
        public:
            virtual void operator()(QGraphicsItem* item, const QRectF& rect)
            {
                DrawObject* rectItem =
                    dynamic_cast<DrawObject*>(item);

                if (rectItem)
                {
                    QPixmap pixmap(rectItem->getSprite()->scaled(rect.width(), rect.height(),Qt::IgnoreAspectRatio));
                    rectItem->setPixmap(pixmap);
                    //rectItem->setRect(rect);
                }
            }
    };

}

GameScene::GameScene(QWidget *parent, int w, int h) : QGraphicsScene(parent)
{
    curObj = NOTDRAWING;

    QPalette Pal(palette());

    Pal.setColor(QPalette::Background, Qt::black);
    this->setPalette(Pal);

    grid = new SceneGrid(w, h, 25);
    this->addItem(grid);

    selectedID = "";
    //selectedFrame = NULL;

    //setMouseTracking(true);

    //scene = new QGraphicsScene(this);
    setSceneRect(0,0,w,h);
}

GameScene::~GameScene()
{
    qDeleteAll(drawMap);
    drawMap.clear();
}

void GameScene::setObjects(objectsScroll *oS)
{
    objectsList = oS;
}

void GameScene::keyReleaseEvent(QKeyEvent *event)
{
    foreach(QGraphicsItem* item, selectedItems())
    {
        DrawObject *tmp = dynamic_cast<DrawObject*>(item);
        if(tmp != NULL && event->key() == Qt::Key_Delete)
        {
            selectedID = tmp->getID();
            drawMap.remove(selectedID);
            selectedID = "";
            removeItem(item);
            delete item;
        }
        else if(tmp != NULL && event->key() == Qt::Key_Space)
        {

        }
    }
    QGraphicsScene::keyReleaseEvent(event);
}

void GameScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    x = event->scenePos().x();
    y = event->scenePos().y();

    emit Mouse_Pos();
    QGraphicsScene::mouseMoveEvent(event);
}

void GameScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        QDateTime timer(QDateTime(QDate(1990,8,3)));
        QString id = QString("COL%1").arg(timer.msecsTo(QDateTime::currentDateTime()));


        int loc_x = event->scenePos().x();
        int loc_y = event->scenePos().y();
        if(curObj != NOTDRAWING)    //drawing object
        {
            QGraphicsView::DragMode vMode = QGraphicsView::NoDrag;
            QGraphicsView* mView = views().at(0);
            if(mView)
                mView->setDragMode(vMode);
            qDebug() << id;
            qDebug() << loc_x << " " << loc_y;

            DrawObject *obj = new DrawObject(id,loc_x,loc_y,objectsList->colObjectVector[curObj],this);
            //obj->setParentItem(dynamic_cast<QGraphicsScene*>(this));
            obj->setPos(loc_x, loc_y);
            obj->setFlag(QGraphicsItem::ItemIsSelectable, true);
            obj->setFlag(QGraphicsItem::ItemIsMovable, true);
            obj->setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
            obj->setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
            addItem(obj);
            drawMap.insert(id,obj);
            qDebug() << obj->pos().x() << " " << obj->pos().y();
            connect(obj, SIGNAL(boom()), this, SLOT(boom()));

            emit Mouse_Left_Pressed();
        }
        else    //selecting object
        {
            /*QGraphicsView::DragMode vMode = QGraphicsView::RubberBandDrag;
            QGraphicsView* mView = views().at(0);
            if(mView)
                mView->setDragMode(vMode);*/

        }
    }
    else if(event->button() == Qt::RightButton) //clearing action
    {
        if(selectedID != "")
        {
            /*if(selectedFrame != NULL)
            {
                removeItem(selectedFrame);
                delete selectedFrame;
                selectedFrame = NULL;
            }*/
            selectedID = "";
        }
        curObj = NOTDRAWING;
        emit Mouse_Right_Pressed();
    }
    QGraphicsScene::mousePressEvent(event);
}

void GameScene::leaveEvent(QEvent *event)
{
    x = 0;
    y = 0;
    emit Mouse_Left();

}

void GameScene::setGrid(int w, int h)
{
    grid->sizeChanged(w,h);
}

void GameScene::setSticky(bool sticky)
{
    this->sticky = sticky;
}

SceneGrid *GameScene::getGrid()
{
    return grid;
}

bool GameScene::isSticky()
{
    return sticky;
}

void GameScene::setCurObject(int i)
{
    curObj = i;
}

void GameScene::boom()
{
    foreach(QGraphicsItem* item, selectedItems())
    {
        DrawObject *tmp = dynamic_cast<DrawObject*>(item);
        if(tmp != NULL)
        {
            qDebug() << tmp->pos().x() << " " << tmp->pos().y();
        }
    }

}
