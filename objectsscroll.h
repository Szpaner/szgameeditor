#ifndef OBJECTSSCROLL_H
#define OBJECTSSCROLL_H

#include <QWidget>
#include <QVector>
#include <QDirIterator>
#include <QDebug>
#include "colobject.h"

class objectsScroll
{

public:
    //ColObject *colOb;
    objectsScroll();
    ~objectsScroll();
    QVector<ColObject*> colObjectVector;
    void reload();
signals:

public slots:
};

#endif // OBJECTSSCROLL_H
