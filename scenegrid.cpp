#include "scenegrid.h"

QRectF SceneGrid::boundingRect() const
{
    return QRectF ( static_cast<qreal>(0), static_cast<qreal>(0), static_cast<qreal>(width), static_cast<qreal>( height));
}

void SceneGrid::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor c (200,200,255,125);

    painter->setPen( c);

    for (int y= 0; y < height; y+=space)
    {
        painter->drawLine(0,y, width, y);
    }

    for (int x= 0; x < width; x+=space)
    {
        painter->drawLine(x,0, x, height);
    }
}

SceneGrid::SceneGrid(int w, int h, int s): QGraphicsItem(), width(w), height(h), space(s)
{

}

void SceneGrid::sizeChanged(int w, int h)
{
    width = w;
    height = h;
}

int SceneGrid::getSpace()
{
     return space;
}
