#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "levelsizedialog.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    selectedAsset = -1;
    ui->setupUi(this);
    ui->objectsHBoxes->setAlignment(Qt::AlignTop);
    ui->objectsTab->setWindowTitle("Objects");
    oS = new objectsScroll();
    gScene = new GameScene(this, 960, 540);
    gScene->setObjects(oS);
    //ui->gameAreaA->setDragMode(QGraphicsView::ScrollHandDrag);
    ui->gameAreaA->setMouseTracking(true);
    ui->gameAreaA->setScene(gScene);
    sigMap = new QSignalMapper(this);
    connect(sigMap, SIGNAL(mapped(int)), this, SLOT(on_clicked(int)));



    //for(QVector::iterator it = oS->colObjectVector.begin(); it != oS->colObjectVector.end(); it++)
    int i = 0;
    foreach (ColObject *c, oS->colObjectVector) {
        QPixmap pixmap(c->icon->scaled(46,46,Qt::KeepAspectRatio));
        QIcon ico(pixmap);
        QPushButton *image = new QPushButton();
        image->setFixedHeight(56);
        image->setFixedWidth(56);
        image->setIcon(ico);
        int wi = pixmap.rect().width();
        int hi = pixmap.rect().height();
        image->setIconSize(QSize(wi,hi));
        sigMap->setMapping(image, i);
        connect(image, SIGNAL(clicked(bool)), sigMap, SLOT(map()));
        objectsLabels.push_back(image);
        ui->objectsHBoxes->addWidget(objectsLabels[i], floor(i/5), i%5);
        i++;
    }
    nAssets = i;
    if(i == 0)
    {
        ui->objectsHBoxes->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
        QLabel *text = new QLabel("No files in folder ASSETS.\nAdd some files to continue.");
        ui->objectsHBoxes->addWidget(text);
        objectsLabels.push_back(text);
    }


    connect(this, SIGNAL(objectsChoice(int)), gScene, SLOT(setCurObject(int)));

    connect(gScene, SIGNAL(Mouse_Pos()), this, SLOT(MouseCurrentPos()));
    connect(gScene, SIGNAL(Mouse_Left_Pressed()), this, SLOT(MousePressed()));
    connect(gScene, SIGNAL(Mouse_Right_Pressed()), this, SLOT(RightMousePressed()));
    connect(gScene, SIGNAL(Mouse_Left()), this, SLOT(MouseLeft()));
}

MainWindow::~MainWindow()
{
    foreach (QWidget *q, objectsLabels)
    {
        delete q;
    }
    objectsLabels.clear();
    delete gScene;
    delete oS;
    delete ui;
}

int MainWindow::getLvlWidth()
{
    return ui->gameAreaA->width();
}

int MainWindow::getLvlHeight()
{
    return gScene->height();
}

void MainWindow::setLvlRect(int w, int h)
{
    gScene->setSceneRect(0,0,w,h);
    gScene->setGrid(w,h);
}

void MainWindow::MouseCurrentPos()
{
    ui->mousePos->setText(QString("(%1,%2)").arg(gScene->x).arg(gScene->y));
}

void MainWindow::RightMousePressed()
{
    if(selectedAsset > -1)
    {
        objectsLabels[selectedAsset]->setStyleSheet("");
        objectsLabels[selectedAsset]->setPalette(style()->standardPalette());

        selectedAsset = -1;
    }
}

void MainWindow::MousePressed()
{

}

void MainWindow::MouseLeft()
{
    ui->mousePos->setText("(0,0)");
}
/*
void MainWindow::newFile()
{

}

void MainWindow::saveFile()
{

}

void MainWindow::saveFileAs()
{

}

void MainWindow::loadFile()
{

}

void MainWindow::quit()
{

}

void MainWindow::createActions()
{

}

void MainWindow::createMenus()
{

}

void MainWindow::contextMenuEvent(QContextMenuEvent *ev)
{

}*/


void MainWindow::on_actionSetLevelSize_triggered()
{
    sizeDialog = new LevelSizeDialog(this);
    sizeDialog->exec();
    delete sizeDialog;
}

void MainWindow::on_actionReload_triggered()
{
    foreach (QWidget *q, objectsLabels)
    {
        ui->objectsHBoxes->removeWidget(q);
        delete q;
    }
    objectsLabels.clear();



    oS->reload();
    int i = 0;
    ui->objectsHBoxes->setAlignment(Qt::AlignTop);
    foreach (ColObject *c, oS->colObjectVector) {
        QPixmap pixmap(c->icon->scaled(46,46,Qt::KeepAspectRatio));
        QIcon ico(pixmap);
        QPushButton *image = new QPushButton();
        image->setFixedHeight(56);
        image->setFixedWidth(56);
        image->setIcon(ico);
        int wi = pixmap.rect().width();
        int hi = pixmap.rect().height();
        image->setIconSize(QSize(wi,hi));
        sigMap->setMapping(image, i);
        connect(image, SIGNAL(clicked(bool)), sigMap, SLOT(map()));
        objectsLabels.push_back(image);
        ui->objectsHBoxes->addWidget(objectsLabels[i], floor(i/5), i%5);
        i++;
    }
    if(i == 0)
    {
        ui->objectsHBoxes->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
        QLabel *text = new QLabel("No files in folder ASSETS.\nAdd some files to continue.");
        ui->objectsHBoxes->addWidget(text);
        objectsLabels.push_back(text);
    }
    nAssets = i;
}

void MainWindow::on_clicked(int i)
{
    RightMousePressed();
    QColor selected(Qt::green);
    QPalette palette;
    palette.setColor(QPalette::Button, selected);
    selectedAsset = i;
    objectsLabels[i]->setAutoFillBackground(true);
    objectsLabels[i]->setPalette(palette);
    objectsLabels[i]->setStyleSheet("background-color: rgb(0,255,0);");
    emit objectsChoice(i);
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::RightButton)
    {
        RightMousePressed();
    }
}


void MainWindow::on_checkBox_clicked()
{
    gScene->setSticky(ui->checkBox->isChecked());
}
