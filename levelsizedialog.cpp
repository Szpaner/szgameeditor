#include "levelsizedialog.h"
#include "sizereduceconfirm.h"

LevelSizeDialog::LevelSizeDialog(MainWindow *parent) : QDialog(parent)
{
    setupUi(this);
    p = parent;
    widthBox->setMaximum(99999);
    heightBox->setMaximum(99999);
    baseW = p->getLvlWidth();
    baseH = p->getLvlHeight();
    widthBox->setValue(baseW);
    heightBox->setValue(baseH);

}

LevelSizeDialog::~LevelSizeDialog()
{

}

void LevelSizeDialog::on_pushOK_clicked()
{
    int curW = widthBox->value();
    int curH = heightBox->value();
    if(curW < baseW || curH < baseH)
    {
        SizeReduceConfirm conf(p, curH, curW);
        conf.exec();
    }
    else
    {
        p->setLvlRect(curW, curH);
    }

    this->close();
}



void LevelSizeDialog::on_pushCancel_clicked()
{
    this->close();
}

