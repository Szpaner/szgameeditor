#include "objectsscroll.h"

objectsScroll::objectsScroll()
{
    //colOb = new ColObject("test1.png");
    QDirIterator assetsDir("ASSETS");
    while(assetsDir.hasNext())
    {
        assetsDir.next();
        if (QFileInfo(assetsDir.filePath()).isFile())
            if (QFileInfo(assetsDir.filePath()).suffix() == "png")
            {
                ColObject *o = new ColObject(assetsDir.filePath());
                colObjectVector.push_back(o);
            }
    }
}

void objectsScroll::reload()
{
    foreach(ColObject *o, colObjectVector)
    {
        delete o;
    }
    colObjectVector.clear();
    QDirIterator assetsDir("ASSETS");
    while(assetsDir.hasNext())
    {
        assetsDir.next();
        if (QFileInfo(assetsDir.filePath()).isFile())
            if (QFileInfo(assetsDir.filePath()).suffix() == "png")
            {
                ColObject *o = new ColObject(assetsDir.filePath());
                colObjectVector.push_back(o);
            }
    }
}

objectsScroll::~objectsScroll()
{
    foreach(ColObject *o, colObjectVector)
    {
        delete o;
    }

    colObjectVector.clear();
}
