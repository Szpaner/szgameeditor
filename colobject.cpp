#include "colobject.h"

ColObject::ColObject(const ColObject &c): QGraphicsItem()
{
    icon = new QPixmap(c.path);
    path = c.path;
    //bounds = new QRectF(*(c.bounds));
    w = c.w;
    h = c.h;
}

ColObject::ColObject(QString p): QGraphicsItem()
{
    icon = new QPixmap(p);
    w = icon->width();
    h = icon->height();
    //bounds = new QRectF(0, 0, w, h);
    path = p;
}

ColObject::ColObject(): QGraphicsItem()
{
    icon = NULL;
}

ColObject::~ColObject()
{
    delete icon;
}

int ColObject::width()
{
    return w;
}

int ColObject::height()
{
    return h;
}

QRectF ColObject::boundingRect() const
{
    return QRectF(0,0,w,h);
}

void ColObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawPixmap(0, 0, w, h, *icon);
    //QGraphicsItem::paint(painter,option,widget);
}

QString ColObject::getPath()
{
    return path;
}
