#include "sizereduceconfirm.h"

SizeReduceConfirm::SizeReduceConfirm(MainWindow *parent, int hi, int wi): QDialog(parent)
{
    setupUi(this);
    h = hi;
    w = wi;
    p = parent;
}

SizeReduceConfirm::~SizeReduceConfirm()
{

}



void SizeReduceConfirm::on_buttonBox_accepted()
{
    p->setLvlRect(w, h);
    this->close();
}

void SizeReduceConfirm::on_buttonBox_rejected()
{
    this->close();
}
