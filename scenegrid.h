#ifndef SCENEGRID_H
#define SCENEGRID_H

#include <QGraphicsItem>
#include <QPainter>
#include <QRectF>

class SceneGrid: public QGraphicsItem
{
private:
    int width;
    int height;
    int space;
    virtual QRectF boundingRect() const;
    virtual void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
public:
    SceneGrid(int w, int h, int s);
    void sizeChanged(int w, int h);
    int getSpace();
};

#endif // SCENEGRID_H
