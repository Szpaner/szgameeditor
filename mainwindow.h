#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QPixmap>
#include <QVector>
#include <QMouseEvent>
#include <QSignalMapper>
#include <QColor>
#include <QPalette>
#include "objectsscroll.h"
#include "gamescene.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //void mouseMoveEvent(QMouseEvent *event);
    int getLvlWidth();
    int getLvlHeight();
    void setLvlRect(int w, int h);
private slots:
    void MouseCurrentPos();
    void MousePressed();
    void MouseLeft();
    void RightMousePressed();
    /*void newFile();
    void saveFile();
    void saveFileAs();
    void loadFile();
    void quit();*/

    void on_actionSetLevelSize_triggered();

    void on_actionReload_triggered();

    void on_clicked(int i);


    void on_checkBox_clicked();

private:
    Ui::MainWindow *ui;
    QDialog *sizeDialog;
    objectsScroll* oS;
    QVector<QWidget*> objectsLabels;
    QSignalMapper *sigMap;
    GameScene *gScene;

    void mousePressEvent(QMouseEvent *event);

    int nAssets;
    int selectedAsset;

    void createActions();
    void createMenus();

protected:
    //void contextMenuEvent(QContextMenuEvent *ev) Q_DECL_OVERRIDE;
signals:
    void objectsChoice(int i);
};

#endif // MAINWINDOW_H
