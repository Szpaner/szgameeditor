#-------------------------------------------------
#
# Project created by QtCreator 2016-06-07T20:27:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SzGameEditorQT
TEMPLATE = app


win32:RC_FILE += icon.rc


SOURCES += main.cpp\
        mainwindow.cpp \
    objectsscroll.cpp \
    colobject.cpp \
    levelsizedialog.cpp \
    sizereduceconfirm.cpp \
    gamescene.cpp \
    selectframe.cpp \
    scenegrid.cpp \
    drawobject.cpp \
    SizeGripItem.cpp

HEADERS  += mainwindow.h \
    objectsscroll.h \
    colobject.h \
    levelsizedialog.h \
    sizereduceconfirm.h \
    gamescene.h \
    selectframe.h \
    scenegrid.h \
    drawobject.h \
    SizeGripItem.h

FORMS    += mainwindow.ui \
    levelsizedialog.ui \
    sizereduceconfirm.ui
