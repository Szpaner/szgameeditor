#ifndef SIZEREDUCECONFIRM_H
#define SIZEREDUCECONFIRM_H

#include <QDialog>
#include "mainwindow.h"
#include "ui_sizereduceconfirm.h"

class SizeReduceConfirm : public QDialog, public Ui::SizeReduceConfirm
{
    Q_OBJECT
    int h;
    int w;
    MainWindow *p;
public:
    SizeReduceConfirm(MainWindow *parent, int hi, int wi);
    ~SizeReduceConfirm();
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
};

#endif // SIZEREDUCECONFIRM_H
