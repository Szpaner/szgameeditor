#ifndef DRAWOBJECT_H
#define DRAWOBJECT_H

#include "colobject.h"
#include <QString>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QPixmap>
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QCursor>
#include <QObject>

class GameScene;


class DrawObject : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    QString id;

    int width;
    int height;
    int x;
    int y;
    QPixmap *sprite;
    QPixmap *curSprite;
    QString path;

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
public:


    DrawObject();
    DrawObject(DrawObject& o);
    DrawObject(QString i, int xx, int yy, ColObject *o, GameScene *parent = NULL);

    // QGraphicsItem interface
    QPixmap *getSprite();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QString getID();
    QString getPath();
    int getWidth();
    int getHeight();


    ~DrawObject();

    void setPixmap(const QPixmap &pixmap);
signals:
    void boom();
};

#endif // DRAWOBJECT_H
